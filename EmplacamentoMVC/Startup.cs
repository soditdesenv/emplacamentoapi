﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmplacamentoMVC.Startup))]
namespace EmplacamentoMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
