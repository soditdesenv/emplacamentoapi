﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmplacamentoAPI.Models
{
    public class Capitais
    {
        public int CapitaisID { get; set; }
        public string Sigla { get; set; }
        public string Estado { get; set; }
        public string Capital { get; set; }
        public string Regiao { get; set; }
        public string Serpro { get; set; }
        public string Ibge { get; set; }
        public string Peso { get; set; }
    }
}