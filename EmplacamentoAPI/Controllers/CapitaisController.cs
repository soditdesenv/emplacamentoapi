﻿using EmplacamentoAPI.DAO;
using EmplacamentoAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EmplacamentoAPI.Controllers
{
    public class CapitaisController : ApiController
    {
        /// <summary>
        /// Autor: Euler Vital
        /// Motivo: Gerencia Capitais
        /// 08/08/2017
        /// </summary>

        CapitaisDAO objDb;

        // GET: api/Capitais
        public IEnumerable<Capitais> Get()
        {
            try
            {
                objDb = new CapitaisDAO();

                return objDb.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST: api/Capitais
        [HttpPost]
        public HttpResponseMessage Post(Capitais objCapitais)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                if (ModelState.IsValid)
                {
                    objDb = new CapitaisDAO();

                    objDb.Set(objCapitais);

                    response = Request.CreateResponse<Capitais>(HttpStatusCode.Created, objCapitais);
                }else
                {
                    var message = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));
                }
            }
            catch
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Ocorreu um erro.");
            }

            return response;
        }

        // PUT: api/Capitais/5
        [HttpPut]
        public HttpResponseMessage Put(int id, Capitais objCapitais)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                if (ModelState.IsValid)
                {
                    objDb = new CapitaisDAO();
                    objCapitais.CapitaisID = id;
                    objDb.Set(objCapitais);

                    response = Request.CreateResponse<Capitais>(HttpStatusCode.Created, objCapitais);
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Ocorreu um erro.");
            }

            return response;
        }

        //// DELETE: api/Capitais/5
        [HttpDelete]
        public void Delete(string id)
        {
        }
    }
}
