﻿using EmplacamentoAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EmplacamentoAPI.DAO
{
    public class CapitaisDAO : DAO
    {
        #region Atributos
        IDataReader dr;
        SqlParameter[] param = null;
        #endregion

        public IEnumerable<Capitais> Get()
        {
            List<Capitais> lista = new List<Capitais>();

            try
            {
                cmd = new SqlCommand();
                dr = ExecReader("USP_SEL_GERENCIAR_CAPITAIS", cmd);

                while (dr.Read())
                {
                    lista.Add(SetarObjeto(dr));
                }
                
            }
            catch(SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { CloseConnection(); }

            return lista;
        }

        public bool Set(Capitais objCapitais)
        {
            bool retorno = false;

            param = new SqlParameter[2];
            cmd = new SqlCommand();

            try
            {

                MontarParametro(0, param, ParameterDirection.Input, "@id", objCapitais.CapitaisID, SqlDbType.Int);
                MontarParametro(1, param, ParameterDirection.Input, "@peso", objCapitais.Peso, SqlDbType.VarChar);

                if (ExecNonQuery("USP_GRAVAR_PESO_CAPITAIS", cmd, param) > 0)
                {
                    retorno = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { CloseConnection();}

            return retorno;
        }


        private Capitais SetarObjeto(IDataReader dr)
        {
            Capitais objCapitais = new Capitais();

            try
            {
                objCapitais.CapitaisID = GetInt32("id", dr);
                objCapitais.Sigla = GetString("sigla", dr);
                objCapitais.Estado = GetString("estado", dr);
                objCapitais.Capital = GetString("capital", dr);
                objCapitais.Regiao = GetString("regiao", dr);
                objCapitais.Serpro = GetString("serpro", dr);
                objCapitais.Ibge = GetString("ibge", dr);
                objCapitais.Peso = dr["peso"].ToString();

                return objCapitais;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}